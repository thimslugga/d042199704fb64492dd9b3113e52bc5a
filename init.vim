" VIM-VSCODE

"*****************************************************************************
" VIM-PLUG CORE
"*****************************************************************************

let vimplug_exists=expand('~/.config/nvim/autoload/plug.vim')
let g:vim_bootstrap_langs = "elixir,eelixir,html,javascript,typescript,python,ruby,yaml"
let g:vim_bootstrap_editor = "nvim"       " nvim or vim


" Encoding
set encoding=utf-8
set fileencodings=utf-8

" Map leader to space
let mapleader=' '

" Insert to Normal mode
inoremap jk <ESC>
inoremap jj <ESC>
inoremap kk <ESC>

" Alt + hjkl move code
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv

" Tab
set smarttab
set tabstop=2
set shiftwidth=2
set sidescroll=1
set softtabstop=0
set shortmess=at

set colorcolumn=120


" always uses spaces instead of tab characters
set expandtab

" Dont wrap horizontally
set nowrap sidescroll=1 listchars=extends:>,precedes:<

" Numbering
"set relativenumber
set number

" don't give |ins-completion-menu| messages.
set shortmess+=c

" always show signcolumns
set signcolumn=yes

" if hidden is not set, TextEdit might fail.
set hidden

" Turn backup off, since most stuff is in SVN, git etc. anyway...
set nobackup
set nowb
set noswapfile

" Syntax
syntax on
filetype plugin indent on

" Set cursorline
set cursorline

" No annoying sound on errors
set noerrorbells
set novisualbell
set tm=500

set redrawtime=10000

" Enable the use of the mouse
set mouse=a

" Minimal number of screen lines to keep above and below the cursor
set scrolloff=8

" Do smart autoindenting when starting a new line
set smartindent

"Override the 'ignorecase' option if the search pattern contains upper case characters.
set smartcase

"When this option is set, the screen will not be redrawn while executing macros, registers and other commands that have not been typed.
set lazyredraw

"A comma separated list of options for Insert mode completion |ins-completion|.
set completeopt=menuone,noinsert,noselect

"Accept clipboard yank
set clipboard+=unnamedplus

"Remove trailing spaces
autocmd BufWritePre * %s/\s\+$//e

" Misc
set t_Co=256
set cindent
set sidescrolloff=1


if !filereadable(vimplug_exists)
  if !executable("curl")
    echoerr "You have to install curl or first install vim-plug yourself!"
    execute "q!"
  endif
  echo "Installing Vim-Plug..."
  echo ""
  silent exec "!\curl -fLo " . vimplug_exists . " --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
  let g:not_finish_vimplug = "yes"

  autocmd VimEnter * PlugInstall
endif

" Required:
call plug#begin(expand('~/.config/nvim/plugged'))

"*****************************************************************************
"" Plug install packages
"*****************************************************************************

"Airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

"Comments
Plug 'scrooloose/nerdcommenter'

"Icons
Plug 'ryanoasis/vim-devicons'

"Git
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'

" Projectionist
Plug 'tpope/vim-projectionist'

" LazyGit
Plug 'kdheepak/lazygit.nvim'

"" Magit
"Plug 'jreybert/vimagit'

"THEMES
"Plug 'morhetz/gruvbox'
Plug 'drewtempelmeyer/palenight.vim'

"Darker contrast onedark
"Plug 'ukyouz/onedark.vim'

" Original onedark
Plug 'joshdick/onedark.vim'

" Elixir special theme (NOT SO GOOD)
"Plug 'embark-theme/vim', { 'as': 'embark', 'branch': 'main' }

" Theme Github
Plug 'projekt0n/github-nvim-theme'

"FZF
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

"COC
Plug 'neoclide/coc.nvim', {'branch': 'release'}

"Tailwindcss
"Plug 'iamcco/coc-tailwindcss',  {'do': 'npm install && npm run build'}

" PR Fix https://github.com/iamcco/coc-tailwindcss/pull/70
" Used @yaegassy/coc-tailwindcss3 instead
"Plug 'yaegassy/coc-tailwindcss',  {'do': 'npm install --frozen-lockfile && npm run build', 'branch': 'feat/support-v3-and-use-server-pkg'}

"Plug 'yaegassy/coc-tailwindcss3', {'do': 'npm install --frozen-lockfile'}

"Snippets
Plug 'honza/vim-snippets'
Plug 'SirVer/ultisnips'

"Svelte
Plug 'othree/html5.vim'
Plug 'pangloss/vim-javascript'
Plug 'evanleck/vim-svelte', {'branch': 'main'}

"Tmux link
Plug 'christoomey/vim-tmux-navigator'

"Extra Formatting
Plug 'chiel92/vim-autoformat'

"Polyglot
Plug 'sheerun/vim-polyglot'

" Earthly
Plug 'earthly/earthly.vim', { 'branch': 'main' }

"HTML EEX formatter
"TODO Install 'beautify-web/js-beautify' with npm

" EasyMotion
Plug 'easymotion/vim-easymotion'

"Better Comments
"Plug 'jbgutierrez/vim-better-comments'
"Plug 'onixus74/vim-better-comments'

Plug 'wakatime/vim-wakatime'

" TS Syntax
Plug 'HerringtonDarkholme/yats.vim'

" Brackets coloring
"Plug 'luochen1990/rainbow'
Plug 'junegunn/rainbow_parentheses.vim'

" Indentation lines
Plug 'Yggdroot/indentLine'

"Smooth Scroll
Plug 'yuttie/comfortable-motion.vim'

"Bookmarks
Plug 'MattesGroeger/vim-bookmarks'

" Search
Plug 'jremmen/vim-ripgrep'


" VIM Syntax Debug
" :SyntaxInfoEnable
Plug 'wadackel/nvim-syntax-info'


" LSP
" Plug 'neovim/nvim-lspconfig'
" Plug 'kabouzeid/nvim-lspinstall'

" NVIM in Browser textbox
" Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }

" MultiCursor
Plug 'mg979/vim-visual-multi', {'branch': 'master'}

" Minimap
"Plug 'severin-lemaignan/vim-minimap'

" Vim Homescreen
"Plug 'mhinz/vim-startify'


" Initialize plugin system
call plug#end()


"! VIM-AIRLINE

"Airline Theme
"let g:airline_theme = 'durant'
"let g:airline_theme = 'onedark'
"let g:airline_theme = 'dark'
"let g:airline_theme = 'badwolf'
"let g:airline_theme = 'deus'
let g:airline_theme = 'palenight'

"Airline seperators
let g:airline_right_sep = ''
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_alt_sep = ''

"let g:airline_left_sep = "\uE0BC"
"let g:airline_right_sep = "\uE0C7"
"let g:airline_left_alt_sep = "\uE0B9"
"let g:airline_right_alt_sep = "\uE0BB"


"Airline Config
let g:airline#extensions#tabline#formatter = 'short_path'

let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#bookmark#enabled = 1


let g:airline#extensions#tagbar#enabled = 1
"let g:airline_skip_empty_sections = 1
let g:airlie_powerline_fonts = 1

" Airline Tabline Config
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_splits = 1
let g:airline#extensions#tabline#switch_buffers_and_tabs = 1

"<silent>let g:airline#extensions#tabline#left_sep = ''
"let g:airline#extensions#tabline#right_sep = ''
"let g:airline#extensions#tabline#left_alt_sep = ''
"let g:airline#extensions#tabline#right_alt_sep = ''


let g:airline#extensions#tabline#left_sep = ''

"let g:airline#extensions#tabline#left_sep = "\uE0B8"
"let g:airline#extensions#tabline#right_sep = "\uE0BA"
"let g:airline#extensions#tabline#left_alt_sep = "\uE0B9"
let g:airline#extensions#tabline#left_alt_sep = ''
let g:airline#extensions#tabline#right_alt_sep = "\uE0BD"

"let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#tabline#fnamecollapse = 1
let g:airline#extensions#tabline#fnametruncate = 0

let g:airline#extensions#tabline#show_close_button = 1
let g:airline#extensions#tabline#close_symbol = 'X'

" !Bookmarks Config
let g:bookmark_sign = ''
let g:bookmark_annotation_sign = ''
"let g:bookmark_highlight_lines = 1


" Tabs
nnoremap K :bnext<CR>
nnoremap J :bpre<CR>
nnoremap <C-S-t> :tabnew<CR>
nnoremap <C-w> :bdelete!<CR>

" Vmap for maintain Visual Mode after shifting > and <
vmap < <gv
vmap > >gv


"Rainbow Config
au VimEnter * RainbowParentheses

"augroup rainbow_lisp
  "autocmd!
  "autocmd FileType elixir,eelixir,javascript,typescript,lisp,clojure,scheme RainbowParentheses
  "autocmd FileType scss, css
"augroup END

let g:rainbow#max_level = 10
let g:rainbow#pairs = [['(', ')'], ['[', ']'], ['{', '}']]

" List of colors that you do not want. ANSI code or #RRGGBB
let g:rainbow#blacklist = [145, 9, 14, 248, 130, 33, 15, 59, 239, 238]

"luochen1990/rainbow NOT WORKING in NVIM 0.5 ()
"let g:rainbow_active = 1
"let g:rainbow_conf = {
"\    'guifgs': ['LightYellow', 'LightCyan', 'LightBlue'],
"\    'ctermfgs': ['lightyellow', 'lightcyan', 'lightmagenta']
"\}


" Visual Multi Cursor Config
let g:VM_maps = {}
let g:VM_maps['Find Under']         = '<C-c>'           " replace C-n
let g:VM_maps['Find Subword Under'] = '<C-c>'           " replace visual C-n

" Indent Line Config
let g:indentLine_setColors = 1
let g:indentLine_char_list = ['┆', '¦', '|']
let g:indentLine_fileTypeExclude = ['coc-explorer']

" Split
noremap <Leader>h :<C-u>split<CR>
noremap <Leader>v :<C-u>vsplit<CR>

" Resize splits
" TODO
nnoremap <leader>+ :vertical resize +5<CR>
nnoremap <leader>- :vertical resize -5<CR>
nnoremap <leader>rp :resize 100<CR>

" Toggle Spell-check
map <leader>o :setlocal spell! spelllang=en_us<CR>


" Svelte Config
" Default to typescript
let g:svelte_preprocessors = ['typescript']


" Vim-Autoformat
" Disabled
" au BufWrite * :Autoformat

" VIM Autoformat Only Elixir EEx
" On Save (Disabled)
"au BufWrite *.html.*eex :Autoformat
" With leader =
nnoremap <leader> = :Autoformat<cr>

let g:autoformat_autoindent = 0
let g:autoformat_retab = 0
let g:autoformat_remove_trailing_spaces = 0


" Elixir HTML EEX Formatter
" INSTALL 'htmlbeautifier' with 'gem install htmlbeautifier' fork onixus74/htmlbeautifier
"let g:formatdef_htmlbeautifier = "'html-beautify -s 2 -I --type html'"
" let g:formatdef_htmlbeautifier = "'htmlbeautifier -b 1 -n eex'"
"let g:formatdef_htmlbeautifier = "'htmlbeautifier -b 1'"

" EEX BEAUTIFIER
"let g:formatdef_htmlbeautifier = "'eexbeautifier -b 1 -e'"
"let g:formatters_eelixir = ['htmlbeautifier']


" JS-BEAUTIFY (HTML-BEAUTIFY)
" https://github.com/beautify-web/js-beautify
"let g:formatdef_htmlbeautifier = "'html-beautify -H -s 2 -m 1 -I -A preserve-aligned -U [\"% end %\", \"% else %\"] --templating php -f -'"
let g:formatdef_htmlbeautifier = '"html-beautify -H -s 2 -m 1 -I -H -A preserve-aligned --templating auto -f -"'
let g:formatters_eelixir = ['htmlbeautifier']

"WIP
"let g:formatdef_htmlbeautifier = "'../../Desktop/_workspace/_lab/_temp/_onixus74/js-beautify/js/bin/html-beautify.js -s 2 -m 1 -I -A preserve-aligned --templating php'"
"let g:formatters_eelixir = ['htmlbeautifier']

au BufNewFile,BufRead, *.eex,*.heex,*.leex,*.sface,*.lexs set filetype=eelixir
"au BufNewFile,BufRead *.eex,*.heex,*.leex,*.sface,*.lexs set filetype=html

" Fix [https://github.com/elixir-editors/vim-elixir/pull/557]
au BufEnter *.heex syn region  surfaceExpression matchgroup=surfaceDelimiter start="{" end="}" contains=@elixirTop  containedin=ALLBUT,@eelixirRegions keepend
au BufEnter *.heex syn region  surfaceExpression matchgroup=surfaceDelimiter start="{" end="}" skip="#{[^}]*}" contains=@elixirTop  containedin=htmlValue keepend

" EasyMotion
"map <Leader><Leader> <Plug>(easymotion-prefix)


"*****************************************************************************
" Abbreviations
"*****************************************************************************
" no one is really happy until you have this shortcuts
cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qall qall



" from readme

if exists('$TMUX')
  " tmux will only forward escape sequences to the terminal if surrounded by a DCS sequence
  let &t_SI .= "\<Esc>Ptmux;\<Esc>\<Esc>[3 q\<Esc>\\"
  let &t_EI .= "\Esc><Ptmux;\<Esc>\<Esc>[1 q\<Esc\\"
  autocmd VimLeave * silent !echo -ne "\033Ptmux;\033\033[0 q\033\\"
else
  let &t_SI .= "\<Esc>[3 q"
  let &t_EI .= "\<Esc>[1 q"
  autocmd VimLeave * silent !echo -ne "\033[0 q"
endif


"! COC config
"
let g:coc_global_extensions = [
      \ 'coc-marketplace',
      \ 'coc-snippets',
      "\ 'coc-tabnine',
      \ 'coc-explorer',
      \ 'coc-fzf-preview',
      \ 'coc-pairs',
      \ 'coc-tsserver',
      \ 'coc-eslint',
      \ 'coc-html',
      \ 'coc-emmet',
      \ 'coc-yaml',
      \ 'coc-css',
      \ 'coc-cssmodules',
      \ 'coc-scssmodules',
      \ 'coc-stylelint',
      \ 'coc-prettier',
      \ 'coc-json',
      "\ 'coc-elixir', "USE ElixirLS directly
      \ 'coc-svelte',
      "\ 'coc-tailwindcss' "Not maintained,
      \ '@yaegassy/coc-tailwindcss3',
      \ 'coc-flutter',
      \ 'coc-markdownlint',
      \ 'coc-markdown-preview-enhanced',
      \ 'coc-webview',
      \ 'coc-project',
      "\ 'coc-bootstrap-classname', "TEMPORARLY Disabled
      \ 'coc-swagger',
      \ ]

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <tab> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
"inoremap <expr> <tab> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

inoremap <expr> <tab> pumvisible() ? "\<C-y>" : "\<C-g><C-g>\<CR>"
"
" Or use `complete_info` if your vim support it, like:
"inoremap <expr> <tab> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use gk to show documentation in preview window
nnoremap <silent> gk :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <F2> <Plug>(coc-rename)

" Remap for format selected region
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)

" Fix autofix problem of current line
xmap <leader>t  <Plug>(coc-format-selected)
nmap <leader>qf  <Plug>(coc-fix-current)

" Create mappings for function text object, requires document symbols feature of languageserver.
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)

" Use <C-d> for select selections ranges, needs server support, like: coc-tsserver, coc-python
" nmap <silent> <C-d> <Plug>(coc-range-select)
" xmap <silent> <C-d> <Plug>(coc-range-select)

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call CocAction('fold', <f-args>)

" use `:OR` for organize import of current buffer
command! -nargs=0 OR :call CocAction('runCommand', 'editor.action.organizeImport')

" Add status line support, for integration with other plugin, checkout `:h coc-status`
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

"Coc-Explorer

" Standard
"nmap <C-n> :CocCommand explorer<CR>
nmap <C-n> :CocCommand explorer --width 45<CR>

" With preset
"nmap <C-n> :CocCommand explorer --preset floatingLeftside<CR>

let g:coc_explorer_global_presets = {
\   '.vim': {
\     'root-uri': '~/.vim',
\   },
\   'cocConfig': {
\      'root-uri': '~/.config/coc',
\   },
\   'tab': {
\     'position': 'tab',
\     'quit-on-open': v:true,
\   },
\   'tab:$': {
\     'position': 'tab:$',
\     'quit-on-open': v:true,
\   },
\   'floating': {
\     'position': 'floating',
\     'open-action-strategy': 'sourceWindow',
\   },
\   'floatingTop': {
\     'position': 'floating',
\     'floating-position': 'center-top',
\     'open-action-strategy': 'sourceWindow',
\   },
\   'floatingLeftside': {
\     'position': 'floating',
\     'floating-position': 'left-center',
\     'floating-width': 70,
\     'open-action-strategy': 'sourceWindow',
\   },
\   'floatingRightside': {
\     'position': 'floating',
\     'floating-position': 'right-center',
\     'floating-width': 50,
\     'open-action-strategy': 'sourceWindow',
\   },
\   'simplify': {
\     'file-child-template': '[selection | clip | 1] [indent][icon | 1] [filename omitCenter 1]'
\   },
\   'buffer': {
\     'sources': [{'name': 'buffer', 'expand': v:true}]
\   },
\ }

" Coc-Prettier
command! -nargs=0 Prettier :call CocAction('runCommand', 'prettier.formatFile')

" TESTING
"au FileType eelixir nnoremap <leader>f :silent %!prettier --print-width=120 --stdin-filepath  %<CR>

" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>

" Show commands
" nnoremap <silent> <space>c  :<C-u>CocList commands<cr>

" Find symbol of current document
"nnoremap <silent> <leader>co  :<C-u>CocFzfList outline<CR>
"nnoremap <silent> <leader>oo  :<C-u>CocFzfList outline<CR>
nnoremap <silent> <leader>oo  :<C-u>CocList outline<cr>


" Search workspace symbols
nnoremap <silent> <leader>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <leader>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <leader>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <leader>p  :<C-u>CocListResume<CR>

"!FZF
" Files
nnoremap <silent> <C-g> :GFiles?<CR>
nnoremap <silent> <C-p> :GFiles<CR>
"nnoremap <silent> <C-P> :Files<CR>

nnoremap <silent> <leader>ff :Files<CR>
nnoremap <silent> <leader>FF :GFiles<CR>

nnoremap <silent> <leader>LL :Lines<CR>
nmap <silent> <leader>ll :BLines<CR>

nnoremap <silent> <leader>II :Commits<CR>
nnoremap <silent> <leader>ii :BCommits<CR>

"WIP
nnoremap <silent> <leader>bb :Buffers<CR>
nnoremap <silent> <leader>mm :CocCommand fzf-preview.Bookmarks <CR>
"nnoremap <silent> <leader>mm :Marks<CR>
"nnoremap <silent> <leader>bb :Marks<CR>

let g:fzf_layout = { 'window': { 'width': 0.95, 'height': 0.95 } }
let $FZF_DEFAULT_OPTS='--reverse'
let g:fzf_preview_window = ['down:75%', 'ctrl-/']

" RipGrep
let g:rg_command = 'rg --vimgrep -S'

" :Rgf Fzf preview RipGrep
command! -bang -nargs=* Rgf
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --color=always --smart-case -- '.shellescape(<q-args>), 1,
  \   fzf#vim#with_preview(), <bang>0)

" Git
" TODO
nmap <leader>gs :G<CR>

" Git diff resolve conflict
nmap <leader>gd :Gvdiffsplit!<CR>
nmap <leader>gj :diffget //2<CR>
nmap <leader>gk :diffget //3<CR>

nnoremap <leader>gf :GFiles<CR>
"nnoremap <leader>gp :Gpush<CR>
"nnoremap <leader>gb :GBranches<CR>
nnoremap <leader>ga :Git fetch --all<CR>
nnoremap <leader>grum :Git rebase upstream main<CR>
nnoremap <leader>grom :Git rebase origin main<CR>
nnoremap <leader>grh :Git rebase -i HEAD~15<CR>


nnoremap <leader>gl :0GcLog<CR>

" !GitGutter
" ToggleHighlight
nnoremap <leader>gi :GitGutterLineHighlightsToggle<CR>

" Preview Hunk
nnoremap <leader>gp :GitGutterPreviewHunk<CR>

" LazyGit
nnoremap <silent> <leader>gg :LazyGit<CR>

" Flutter
let g:dart_format_on_save = 1

let g:dart_style_guide = 2

" !VIMRC
" TODO
" Open config file from anywhere
"nnoremap <leader>rc :e $MYVIMRC<CR>

" Source config
nnoremap <leader>so :source ~/.config/nvim/init.vim<cr>

" Automatically source vimrc on save.
" autocmd! bufwritepost ~/.config/nvim/init.vim source %

" ENVRC
au BufRead,BufNewFile *.env* set filetype=sh

" Theme
"
" Note :
" github_dark + palenight good
" github_dark + onedark good

"colorscheme gruvbox
colorscheme github_dark
colorscheme palenight
"colorscheme onedark
"colorscheme nvcode


set background=dark

